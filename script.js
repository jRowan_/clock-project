console.log("Hey");

let canvas = document.getElementById("the_canvas");
let context = canvas.getContext("2d");
let date = new Date();

//let circleSecs = document.getElementById("circleSec");



let SecondsCircleX = 240;
let MinutesCircleX = 510;
let HoursCircleX = 820;
let DaysCircleX = 1168;
let YearsCircleX = 1600;

let Scl1 = '#f9d592';
let Scl2 = '#ffb7ff';

let Mcl1 = '#ffc095';
let Mcl2 = '#d689ff';

let Hcl1 = '#f4aa91';
let Hcl2 = '#b66ee8';

let Dcl1 = '#f49595';
let Dcl2 = '#a564d3';

let Ycl1 = '#ee8096';
let Ycl2 = '#c77dff';

let Bgcl1 = '#ffd497';
let Bgcl2 = '#7251b5';

let SecondCirceColour = Scl1;
let MinuteCircleColour = Mcl1;
let HourCircleColour = Hcl1;
let DayCircleColour = Dcl1;
let YearCircleColour = Ycl1;


let backgroundColour = Bgcl1;



const hour = date.getHours();
console.log(hour);

if(hour < 7){
    
    SecondCirceColour = Scl2;
    MinuteCircleColour = Mcl2;
    HourCircleColour = Hcl2;
    DayCircleColour = Dcl2;
    YearCircleColour = Ycl2; 
    backgroundColour = Bgcl2; 
}

if(hour > 18){
    
    SecondCirceColour = Scl2;
    MinuteCircleColour = Mcl2;
    HourCircleColour = Hcl2;
    DayCircleColour = Dcl2;
    YearCircleColour = Ycl2;
    backgroundColour = Bgcl2;
}








/////--------------------------------------------------


let circleSec = new GameObject(100, 100, 20);

let circleMin = new GameObject(100, 100, 20);

let circleHour = new GameObject(100, 100, 20);

let circleDay = new GameObject(100, 100, 20);



circleSec.x = 450;
circleSec.y = 500;

circleMin.x = 1200;
circleMin.y = 500;



circleHour.x = 1200;
circleHour.y = 500;

circleDay.x = 1200;
circleDay.y = 500;

let audioP = new Audio;

audioP.src = "pop.mp3";


///=======================================================


let bigCircleSecs = new GameObject(100, 100, 50);
let bigCircleMin = new GameObject(100, 100, 50);
let bigCircleHour = new GameObject(100, 100, 50);
let bigCircleDay = new GameObject(100, 100, 50);
let bigCircleYear = new GameObject(100, 100, 50);



bigCircleSecs.x = SecondsCircleX;
bigCircleSecs.y = 500;

bigCircleMin.x = MinutesCircleX;
bigCircleMin.y = 500;

bigCircleHour.x = HoursCircleX;
bigCircleHour.y = 500;

bigCircleDay.x = DaysCircleX;
bigCircleDay.y = 500;

bigCircleYear.x = YearsCircleX;
bigCircleYear.y = 500;



function GameObject(x, y, size,){
    this.x = x;
    this.y = y;
    this.size = size;
}


function drawlineOne(){

    context.beginPath();
    context.moveTo(SecondsCircleX, 500);
    context.lineTo(MinutesCircleX, 500);
    context.lineWidth = 24;
    context.stroke();
    
    
}



function drawlineTwo(){

   // context.beginPath();
    context.moveTo(MinutesCircleX, 500);
    context.lineTo(HoursCircleX, 500);
    context.lineWidth = 24;
    context.stroke();
}

// function drawlineThree(){

//     context.beginPath();
//     context.moveTo(MiliSecCircleX, 500);
//     context.lineTo(SecondsCircleX, 500);
//     context.lineWidth = 5;
//     context.stroke();
// }

function drawlineFour(){

    context.beginPath();
    context.moveTo(HoursCircleX, 500);
    context.lineTo(DaysCircleX, 500);
    context.lineWidth = 24;
    context.stroke();
}

function drawlineFive(){

    context.beginPath();
    context.moveTo(DaysCircleX, 500);
    context.lineTo(YearsCircleX, 500);
    context.lineWidth = 24;
    context.stroke();
}




function draw(){

    context.fillStyle = 'black';

    context.fillStyle = backgroundColour;
    context.fillRect(0,0,canvas.width,canvas.height);
    
    drawlineOne();
    drawlineTwo();
    //drawlineThree();
    drawlineFour();
    drawlineFive();

   
  // context.strokeStyle = '#d41717';
//-------------------------------------------------------------------------------------------------------------------------------
   



//---------------------------------------------------------------------------------------------------------------------------------
    
    context.fillStyle = SecondCirceColour;
    context.beginPath();
    context.arc(circleSec.x, circleSec.y, circleSec.size, 0, 2 * Math.PI);                      //MOVING SECS
    context.fill();

    
    context.fillStyle = SecondCirceColour;
    context.strokeStyle = "white";
    context.lineWidth = 30;
    context.beginPath();
    context.arc(bigCircleSecs.x, bigCircleSecs.y, bigCircleSecs.size * Math.cos((date.getMilliseconds() - 500) / 1000) , 0, 2 * Math.PI);      //STATIC SECS
    context.stroke();
    context.fill();
   
//---------------------------------------------------------------------------------------------------------------------------------- 
    context.beginPath();
    context.fillStyle = MinuteCircleColour;
    context.fillStyle = 'ee5d6c';
    context.arc(circleMin.x, circleMin.y, circleMin.size, 0, 2 * Math.PI);         //MOVING MINS
    context.fill();

    if(date.getSeconds == 0)
    {
        context.fillStyle = MinuteCircleColour;
        context.strokeStyle = "A0BFE0";
        context.lineWidth = 30;
        context.beginPath();
        context.arc(bigCircleMin.x, bigCircleMin.y,bigCircleMin.size + bigCircleMin.size - bigCircleMin.size * (date.getMilliseconds() / 1000), 0, 2 * Math.PI);     //STATIC MINS
        context.stroke();
        context.fill();
    }
    else
    {
        context.fillStyle = MinuteCircleColour; 
        context.strokeStyle = "white";
        context.lineWidth = 30;
        context.beginPath();
        context.arc(bigCircleMin.x, bigCircleMin.y,bigCircleMin.size + bigCircleMin.size * (date.getSeconds() / 60), 0, 2 * Math.PI);     //STATIC MINS
        context.stroke();
        context.fill();
    }

//---------------------------------------------------------------------------------------------------------------------------------
    context.beginPath();
    context.arc(circleHour.x, circleHour.y, circleHour.size, 0, 2 * Math.PI);     //MOVING CIRLCE HOURS
    context.fillStyle = HourCircleColour;
    context.fill();

    context.fillStyle = HourCircleColour;
    context.strokeStyle = "white";
    context.lineWidth = 30;
    context.beginPath();
    context.arc(bigCircleHour.x, bigCircleHour.y, bigCircleHour.size + bigCircleHour.size * (date.getMinutes() / 60), 0, 2 * Math.PI);     //STATIC HOUR
    context.stroke();
    context.fill();

    //  context.beginPath();
    //  context.arc(HoursCircleX, 500, 90, 0, 2 * Math.PI, false);
    //  context.fillStyle = '#EF9595';
    //  context.fill();
    //  context.lineWidth = 5;
    //  context.strokeStyle = "white";
    //  context.stroke();
//----------------------------------------------------------------------------------------------------------------------------------
    context.beginPath();
    context.arc(circleDay.x, circleDay.y, circleDay.size, 0, 2 * Math.PI);     //MOVING CIRLCE HOURS
    context.fillStyle = DayCircleColour;
    context.fill();


    context.fillStyle = DayCircleColour;
    context.strokeStyle = "white";
    context.lineWidth = 30;
    context.beginPath();
    context.arc(bigCircleDay.x, bigCircleDay.y, bigCircleDay.size + bigCircleDay.size * (date.getHours() / 24), 0, 2 * Math.PI);     //STATIC HOUR
    context.stroke();
    context.fill();

   
//-----------------------------------------------------------------------------------------------------------------------------------
    var now = new Date();
    var start = new Date(now.getFullYear(), 0, 0);
    var diff = now - start;                                              //someone elses code lol (stackoverflow)
    var oneDay = 1000 * 60 * 60 * 24;
    var day = Math.floor(diff / oneDay);

    context.fillStyle = YearCircleColour;
     context.strokeStyle = "white";
     context.lineWidth = 30;
     context.beginPath();
     context.arc(bigCircleYear.x, bigCircleYear.y, bigCircleYear.size + bigCircleYear.size * (day / 365), 0, 2 * Math.PI);     //STATIC HOUR
     context.stroke();
     context.fill();

    
}
//date.setMinutes(59);
function update(){
    //date.setMilliseconds(date.getMilliseconds()+500);
   
    //moveMiliseconds();

date = new Date;

    circleSec.x = SecondsCircleX + (MinutesCircleX - SecondsCircleX) * (date.getMilliseconds() / 1000); //seconds


    if(date.getMilliseconds() < 100)
    {
        audioP.play();
    }
    
    


    if(date.getSeconds() == 0)
    {
        circleMin.x = MinutesCircleX + (HoursCircleX - MinutesCircleX) * (date.getMilliseconds() / 1000); //minutes
    }

    // context.font = "100px Arial";
    // context.fillStyle = 'black';
    // context.fillText(date.toLocaleTimeString()+"."+ date.getMilliseconds(), 0,100);

    if(date.getMinutes() == 0 && date.getSeconds() == 0)
    circleHour.x = HoursCircleX + (DaysCircleX - HoursCircleX) * (date.getMilliseconds() / 1000); //hours

    if(date.getMinutes() == 0 && date.getSeconds() == 0 && date.getHours == 0)
    circleHour.x = HoursCircleX + (DaysCircleX - HoursCircleX) * (date.getMilliseconds() / 1000); //days

    if(date.getMinutes() == 0 && date.getSeconds() == 0 && date.getHours == 0 && date.getFullYear() == 0)
    circleDay.x = DaysCircleX + (YearsCircleX - DaysCircleX) * (date.getMilliseconds() / 1000); //years

   
}










function moveSeconds(){
    circleSec.x = circleSec.x + 1;

    if(circleSec.x <= MinutesCircleX){
                                                    //SECONDS
        circleSec.x += 1;
    }
     else{
         circleSec.x = SecondsCircleX;
        
    }
}

function moveMinutes(){

    circleMin.x = circleMin.x + 1;

    if(circleMin.x <= HoursCircleX){
    
        circleMin.x += 1;                           //MINUTES
    }
     else{
        circleMin.x = MinutesCircleX;
        
    }
}

function moveHours(){

    circleHour.x = circleHour.x + 1;

if(circleHour.x <= DaysCircleX){               //HOURS

    circleHour.x += 1;
}
 else{
    circleHour.x = HoursCircleX;
    
}
}

function moveDays(){
    circleDay.x = circleDay.x + 1;

if(circleDay.x <= YearsCircleX){               //HOURS

    circleDay.x += 1;
}
 else{
    circleDay.x = DaysCircleX;
    
}
}



  function render(){
    draw();
    update();
    window.requestAnimationFrame(render);
  //  console.log("grr");
    //context.clearRect(0,0, canvas.width, canvas.height);
}

    canvas.width = window.innerWidth;
    canvas.height = window.innerHeight;

  window.requestAnimationFrame(render); 
